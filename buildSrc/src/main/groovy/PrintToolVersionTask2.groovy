import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class PrintToolVersionTask2 extends DefaultTask{
    String tool

    @TaskAction
    void printToolVersion() {
        switch (tool) {
            case 'java':
                println 'HELLO THIS IS JAVA'
                break
            case 'groovy':
                println 'HELLO THIS IS GROOVY'
                break
            default:
                throw new IllegalArgumentException("Unknown tool")
        }
    }
}
